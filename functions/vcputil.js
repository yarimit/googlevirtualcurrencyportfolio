var AWS = require('aws-sdk');

function VcpUtil() {
    this.documentClient = new AWS.DynamoDB.DocumentClient();
    this.tableName = "AlexaVirtualCurrencyPortfolioTable";
}

VcpUtil.prototype = {
    getAttribute: function(app) {
        return new Promise((resolve, reject) => {
            this.documentClient.get({
                TableName : this.tableName,
                Key: {
                    userId: `Google-${app.getUser().userId}`
                }
            }, (err, data) => {
                if(err) {
                    resolve({});
                } else {
                    if(data.Item && data.Item.mapAttr) {
                        //console.log(`getAttribute ${JSON.stringify(data.Item)}`);
                        resolve(data.Item.mapAttr);
                    } else {
                        resolve({});
                    }
                }
            });
        });
    },
    getWallet: function(app) {
        return new Promise((resolve, reject) => {
            this.getAttribute(app).then((data) => {
                resolve(data['wallet']);
            }, (err) => {
                resolve({});
            });
        });
    },
    saveAttribute: function(app, attributes) {
        return new Promise((resolve, reject) => {
            this.documentClient.put({
                TableName : this.tableName,
                Item: {
                    "userId" : `Google-${app.getUser().userId}`,
                    "mapAttr": attributes
                }
            }, function(err, data) {
                if(err) {
                    console.error(`Failed. ${err}`);
                    reject(err);
                } else {
                    resolve(null);
                }
            });        
        });
    }
}

exports.VcpUtil = VcpUtil;