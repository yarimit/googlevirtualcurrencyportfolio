// Copyright 2016, Google, Inc.
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

'use strict';

process.env.DEBUG = 'actions-on-google:*';
const { DialogflowApp } = require('actions-on-google');
const functions = require('firebase-functions');

var AWS = require('aws-sdk');

// コイン定義/レート取得API
const VC = require('./virtual-currency.js');
const coins = VC.COINS;

// 過去レート取得
const DIFF_HOURS = 24;
const PREV_DESC = "前日";
const RH = require('./rate-history.js');

// DynamoDB Attribute保存Util
const VCP = require('./vcputil.js');

// ステートの定義
const states = {
    GETDETAIL: "_GETDETAIL",
    UPDATEWALLET_INT: "_UPDATEWALLET_INT",
    UPDATEWALLET_DEC: "_UPDATEWALLET_DEC",
    UPDATEWALLET_CONFIRM: "_UPDATEWALLET_CONFIRM"
};

function lookupCoin(coinSlot) {
    return coins.find((c) => {
        return (c.name === coinSlot ||
            c.alias.indexOf(coinSlot) > -1);
    });
}

function logPrefix(app, intent) {
    try {
        return `${intent}:UID(${app.getUser().userId}) `;
    } catch (error) {
        console.error("logPrefix: Can't get UserID");
        return `${intent}:UID(unknown)`;
    }
}

function currencyFormat(num) {
    var delimiter = '.',
        numString = num.toString(),
        delimitExp = new RegExp('(\\d)(?=(\\d{3})+$)', 'g'), // 整数フォーマット。
        decimalDelimitExp = new RegExp('(\\d)(?=(\\d{3})+(\\.\\d+))', 'g'); // 小数フォーマット。

    function isDecimal() {
        if (numString.indexOf(delimiter) > -1) {
            return true;
        } else {
            return false;
        }
    }

    if (isDecimal()) {
        return numString.replace(decimalDelimitExp, '$1,');
    } else {
        return numString.replace(delimitExp, '$1,');
    }
}

// コインのレート用メッセージ作成
// {coin}のレートはxxよりxx% {上がって/下がって} xx円で、評価額は xx円です。
// {coin}のレートはxxよりxx% {上がって/下がって} xx円です。
// {coin}のレートはxx円です。
function coinRateMessage(coin, rate, prevRate, wallet) {
    // 現在価格
    var diffMessage = "";
    if (prevRate !== 0) {
        // 前のレートが取れている時だけ騰落率出力
        let d = (rate >= prevRate) ? '上がって' : '下がって';
        let diff = Math.abs(Math.floor(((rate - prevRate) / prevRate) * 100 * 100) / 100);
        diffMessage = `${PREV_DESC}より${diff}％${d}`;
    }
    var message = `${coin.name}のレートは${diffMessage}${rate}円`;

    if (wallet[coin.code]) {
        // 評価額
        let yourcoin = wallet[coin.code];
        let hyouka = Math.floor(yourcoin * rate);
        message += `で、評価額は${hyouka}円です。`;
    } else {
        message += `です。`;
    }
    return message;
}

exports.virutalCurrencyPortfolio = functions.https.onRequest((request, response) => {
    const app = new DialogflowApp({ request, response });
    const userId = app.getUser().userId;
    console.log('Request headers: ' + JSON.stringify(request.headers));
    console.log('Request body: ' + JSON.stringify(request.body));
    console.log(`userId: ${userId}`);

    process.env.AWS_ACCESS_KEY_ID = functions.config().dynamodb.access;
    process.env.AWS_SECRET_ACCESS_KEY = functions.config().dynamodb.secret;
    process.env.AWS_REGION = 'ap-northeast-1';
    AWS.config.update({ region: 'ap-northeast-1' });

    let rateHistory = new RH.RateHistory('VirtualCurrencyRateTable', DIFF_HOURS);
    let vcpUtil = new VCP.VcpUtil();

    const DEFAULT_PROMPT = '総資産を尋ねるか、レートを知りたいコインの名前をおっしゃって下さい。';

    // Fulfill action business logic
    function responseHandler(app) {
        app.ask('仮想通貨ポートフォリオへようこそ。' + DEFAULT_PROMPT);

        vcpUtil.getAttribute(app).then((attr) => {
            attr["STATE"] = undefined;
            attr["detail"] = undefined;

            vcpUtil.saveAttribute(app, attr).then(() => {
            }, (error) => {
            });
        }, (error) => {
        });
    }

    function responseHandlerHelp(app) {
        app.ask('仮想通貨の価格や評価額をチェックします。' +
            'たとえば、「ビットコインのレートを教えて」と聞いてください。' +
            'また、「ビットコインの所持数を変更」などと話しかけて所持コインを設定しておくと' +
            '「総資産を教えて」と聞かれた時に日本円での合計評価額をお伝えします。' +
            DEFAULT_PROMPT
        );
    }

    function responseHandlerGetBalance(app) {
        vcpUtil.getAttribute(app).then((attr) => {
            let wallet = attr['wallet'] || {};

            // 所持コインの種類数
            var coinTypeCnt = coins.filter((coin) => {
                return (wallet[coin.code] && wallet[coin.code] > 0);
            }).length;

            if (coinTypeCnt == 0) {
                // 所持コイン設定なし
                console.warn(`${logPrefix(app, 'GetBalance')} 所持コイン未設定`);
                app.ask(`所持コインが設定されていません。例えば「ビットコインの所持数を変更」のように話しかけてコインの所持数の設定を行ってください。`);
                return;
            }

            // 前のレート取得
            rateHistory.getRateAll().then((prevRates) => {
                // 全過去レートが存在するかフラグ
                var existAllPrevRates = true;

                // 現在のレートを取得して各通貨情報を詰めていく
                new Promise(function (resolve, reject) {
                    var balance = [];
                    coins.forEach((coin) => {
                        if (wallet[coin.code] && wallet[coin.code] > 0) {
                            VC.getRate(coin).then((rate) => {
                                let frate = Math.floor(rate * 100) / 100;
                                let prevRate = prevRates[coin.code] ? Math.floor(prevRates[coin.code] * 100) / 100 : 0;
                                existAllPrevRates = prevRates[coin.code] ? existAllPrevRates : false;
                                var yourcoin = wallet[coin.code];

                                balance.push({
                                    coin: coin,
                                    hasCoin: yourcoin,
                                    rate: frate,
                                    prevRate: prevRate,
                                    jpy: Math.floor(yourcoin * frate),
                                    prevJpy: Math.floor(yourcoin * prevRate)
                                });

                                if (coinTypeCnt == Object(balance).length) {
                                    resolve(balance);
                                }
                            }, (err) => {
                                reject(err);
                            });
                        }
                    });
                }).then((balance) => {
                    // order順にソートしておく
                    balance.sort((a, b) => {
                        return a.coin.order > b.coin.order;
                    });

                    let totalJpy = Math.floor(
                        balance.reduce((a, x) => a += x.jpy, 0)
                    );
                    let totalJpyPrev = Math.floor(
                        balance.reduce((a, x) => a += x.prevJpy, 0)
                    );

                    // 騰落率
                    var diffMessage = '';
                    var diffCardMessage = '';
                    if (existAllPrevRates) {
                        // 全過去レートが存在した時だけ出力
                        let diffSign = (totalJpy >= totalJpyPrev) ? '+' : '-';
                        let diff = Math.abs(Math.floor(((totalJpy - totalJpyPrev) / totalJpyPrev) * 100 * 100) / 100);
                        diffCardMessage = `(${diffSign} ${diff}%)`;

                        let d = (totalJpy >= totalJpyPrev) ? '上がって' : '下がって';
                        diffMessage = `${PREV_DESC}より${diff}％${d}`;
                        console.info(`${logPrefix(app, 'GetBalance')} success. totalJpy=${totalJpy}, diff=${diffSign}${diff}%`);
                    } else {
                        console.info(`${logPrefix(app, 'GetBalance')} success. totalJpy=${totalJpy}, no diff`);
                    }

                    let message = `現在の総資産は${diffMessage}${totalJpy}円です。`;

                    // カードには各通貨詳細を出力
                    let cardTitle = `総資産評価額`;
                    var cardContent = `${currencyFormat(totalJpy)} 円 ${diffCardMessage}\n`;
                    var cardDetails = balance.map((b) => {
                        return `${b.coin.code.toUpperCase()} : ${currencyFormat(b.jpy)} 円 (${currencyFormat(b.rate)} x ${b.hasCoin} coin)`;
                    });
                    cardContent += cardDetails.join('\n');

                    console.info(`${logPrefix(app, 'GetBalance')} success. state -> GETDETAIL`);
                    let getDetailMessage = "各通貨のレートをチェックしますか？";
                    attr["STATE"] = states.GETDETAIL;
                    attr["detail"] = balance;

                    // 使用状況更新
                    attr["totalJpy"] = totalJpy;
                    attr["lastAccess"] = new Date().toISOString();
                    if (attr["accessCount"]) {
                        attr["accessCount"] += 1;
                    } else {
                        attr["accessCount"] = 1;
                    }

                    vcpUtil.saveAttribute(app, attr).then(() => {
                        app.ask(message + getDetailMessage);
                    }, (error) => {
                        console.error(error);
                        app.ask(message + DEFAULT_PROMPT);
                    });

                    // this.emit(':askWithCard',
                    //     message + getDetailMessage,
                    //     getDetailMessage,
                    //     cardTitle,
                    //     cardContent
                    // );
                }, (error) => {
                    console.error(`${logPrefix(app, 'GetBalance')} ${error}`);
                    _this.emit(':ask', `総資産の取得に失敗しました。もう一度レートを知りたいコインの名前をおっしゃって下さい。`);
                });
            });
        });
    }

    function responseHandlerGetBalanceNo(app) {
        vcpUtil.getAttribute(app).then((attr) => {
            if (attr['STATE'] === states.GETDETAIL) {
                app.tell('各通貨レートの確認を中止し、終了します。またのご利用お待ちしております。');

                // ステート他のリセット
                attr['STATE'] = undefined;
                attr['detail'] = undefined;

            } else {
                app.ask('すいません。よくわかりません。');
            }

            vcpUtil.saveAttribute(app, attr).then(() => {
            }, (error) => {
            });
        });
    }

    function responseHandlerGetBalanceYes(app) {
        vcpUtil.getAttribute(app).then((attr) => {
            if (attr['STATE'] === states.GETDETAIL) {
                var detail = attr["detail"];

                // ハンドラの実行後、スキルの初期状態に戻すためステートをリセット
                attr['STATE'] = undefined;
                attr['detail'] = undefined;
    
                var messages = detail.map((d) => {
                    //return `${d.name} のレートは ${d.rate}円、評価額は${d.jpy}円です。`;
                    return coinRateMessage(d.coin, d.rate, d.prevRate, attr["wallet"]);
                });
                var message = messages.join('');
                console.info(`${logPrefix(app, 'GETDETAIL:YesIntent')} success. state -> default`);
    
                app.tell(message + '以上です。またのご利用お待ちしております。');
            } else {
                app.ask('すいません。よくわかりません。');
            }
            

            vcpUtil.saveAttribute(app, attr).then(() => {
            }, (error) => {
            });
        });
    }

    function responseHandlerGetCoin(app) {
        let coin = lookupCoin(app.getArgument('Coin'));
        //const coinStr = JSON.stringify(request.body.result.parameters.Coin);
        //let coin = lookupCoin(coinStr.replace(/\"/g, ""));

        if (!coin) {
            console.warn(`lookupCoin failed [${_coin}]`);
            app.ask('コイン名の認識に失敗しました。もう一度レートを知りたいコインの名前をおっしゃって下さい。');
            return;
        }


        VC.getRate(coin).then((rate) => {
            rateHistory.getRate(coin.code).then((prev_rate) => {
                vcpUtil.getAttribute(app).then((attr) => {

                    // 丸める
                    let frate = Math.floor(rate * 100) / 100;
                    let prevFrate = Math.floor(prev_rate * 100) / 100;

                    // 騰落率
                    var diffSign, diff;
                    var diffCardMessage = ""; // カード出力用騰落率
                    if (prev_rate !== 0) {
                        diffSign = (frate >= prevFrate) ? '+' : '-';
                        diff = Math.abs(Math.floor(((frate - prevFrate) / prevFrate) * 100 * 100) / 100);
                        diffCardMessage = `(${diffSign}${diff}%)`;
                    }

                    let wallet = attr['wallet'] || {};

                    // レスポンス構築
                    let cardTitle = `${coin.name}(${coin.code.toUpperCase()})`;
                    var cardContent = `1${coin.code.toUpperCase()} = ${currencyFormat(frate)}JPY ${diffCardMessage}`;

                    if (wallet[coin.code]) {
                        // 評価額
                        let yourcoin = wallet[coin.code];
                        let hyouka = Math.floor(yourcoin * frate);
                        cardContent += `\n評価額 ${currencyFormat(hyouka)} 円 (${yourcoin} coin)`;
                    }

                    // 使用状況更新
                    attr["lastAccess"] = new Date().toISOString();
                    if (attr["accessCount"]) {
                        attr["accessCount"] += 1;
                    } else {
                        attr["accessCount"] = 1;
                    }

                    let message = coinRateMessage(coin, frate, prevFrate, wallet);
                    console.info(`${logPrefix(app, 'GetCoin')} success. coin.name=${coin.name}, rate=${frate}, prev_rate=${prevFrate}, diff=${diffSign} ${diff}%`);

                    vcpUtil.saveAttribute(app, attr).then(() => {
                        app.ask(message + DEFAULT_PROMPT);
                        //app.tell(`${coin.name}の所持数を${count}に変更しました。`);
                    }, (error) => {
                        console.error(error);
                        app.ask(message + DEFAULT_PROMPT);
                    });

                    /*
                    _this.emit(':askWithCard',
                        message + DEFAULT_PROMPT,
                        DEFAULT_PROMPT,
                        cardTitle, cardContent);
                    */
                });
            });
        }, (error) => {
            console.error(`${logPrefix(app, 'GetCoin')} ${error}`);
            //_this.emit(':ask', `${coin.name}のレート取得に失敗しました。もう一度レートを知りたいコインの名前をおっしゃって下さい。`);
            app.ask(`${coin.name}のレート取得に失敗しました。もう一度レートを知りたいコインの名前をおっしゃって下さい。`);
        });
    }

    function responseHandlerUpdateWallet(app) {
        const coinStr = JSON.stringify(request.body.result.parameters.Coin);
        const count = app.getArgument('Count');
        let coin = lookupCoin(app.getArgument('Coin'));

        if (!coin) {
            console.warn(`lookupCoin failed [${coin}]`);
            app.ask('コイン名の認識に失敗しました。もう一度レートを知りたいコインの名前をおっしゃって下さい。');
            return;
        }

        if (Number.isNaN(count)) {
            console.warn(`invalid number [${count}]`);
            app.tell('数値が認識できませんでした。');
            return;
        }

        vcpUtil.getAttribute(app).then((attr) => {
            let wallet = attr['wallet'] || {};
            wallet[coin.code] = Number(count);
            attr['wallet'] = wallet;

            vcpUtil.saveAttribute(app, attr).then(() => {
                app.ask(`${coin.name}の所持数を${count}に変更しました。` + DEFAULT_PROMPT);
            }, (error) => {
                console.error(error);
                app.tell('保存に失敗しました。');
            });
        }, (error) => {
            console.error(error);
            app.tell('読み込みに失敗しました。');
        });
    }

    function responseHandlerStop(app) {
        app.tell('さようなら、またのご利用をお待ちしております。');

        vcpUtil.getAttribute(app).then((attr) => {
            attr["STATE"] = undefined;
            attr["detail"] = undefined;

            vcpUtil.saveAttribute(app, attr).then(() => {
            }, (error) => {
            });
        }, (error) => {
        });
    }

    const actionMap = new Map();
    actionMap.set('Welcome', responseHandler);
    actionMap.set('GetBalanceAction', responseHandlerGetBalance);
    actionMap.set('NoAction', responseHandlerGetBalanceNo);
    actionMap.set('YesAction', responseHandlerGetBalanceYes);

    actionMap.set('GetCoinAction', responseHandlerGetCoin);
    actionMap.set('UpdateWalletAction', responseHandlerUpdateWallet);
    actionMap.set('HelpAction', responseHandlerHelp);
    actionMap.set('StopAction', responseHandlerStop);

    app.handleRequest(actionMap);
});