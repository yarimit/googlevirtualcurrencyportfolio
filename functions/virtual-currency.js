// CoinCheckレート取得
var CoinCheck = require('./coincheck-node/src/coin_check.js');
var coinCheck = new CoinCheck.CoinCheck('', '');
function getRateCoinCheck(coin) {
    return new Promise(function (resolve, reject) {
        coinCheck.ticker.rate({
            options: {
                error: (error, response, params) => {
                    console.error('error', error);
                    reject(error);
                },
                success: (data, response, params) => {
                    var rate = (JSON.parse(data)).rate;
                    resolve(rate);
                }
            }
        }, coin.code + "_jpy");
    });
};

// Zaifレート取得
const https = require('https');
function getRateZaif(coin) {
    const API_PATH = '/api/1/last_price';
    const API_HOST = 'api.zaif.jp';
    return new Promise(function (resolve, reject) {
        let req_params = {
            host: API_HOST,
            port: 443,
            path: `${API_PATH}/${coin.code}_jpy`,
            method: "get",
            headers: {
                'Content-Type': 'application/json'
            }
        };

        req = https.request(req_params, function (response) {
            /*
            console.info('headers:', response.headers);
            console.info('statusCode:', response.statusCode);
            */
            var data = '';

            //another chunk of data has been recieved, so append it to `str`
            response.on('data', function (chunk) {
                data += chunk;
            });

            //the whole response has been recieved, so we just print it out here
            response.on('end', function () {
                if (response.statusCode == 200) {
                    var rate = (JSON.parse(data)).last_price;
                    resolve(rate);
                } else {
                    console.error('error', data);
                    reject(data);
                }
            });
        });
        req.on('error', function (error) {
            console.error('error', error);
            reject(error);
        });
        req.end();
    });
}

// コイン定義
exports.COINS = [
    {
        name: "ビットコイン",
        alias: ["ビット"],
        code: "btc",
        order: 1,
        getRate: getRateCoinCheck
    },
    {
        name: "イーサリアム",
        alias: ["イーサ"],
        code: "eth",
        order: 2,
        getRate: getRateCoinCheck
    },
    {
        name: "イーサリアムクラシック",
        alias: ["イークラ", "イーサクラシック"],
        code: "etc",
        order: 3,
        getRate: getRateCoinCheck
    },
    {
        name: "リスク",
        alias: [],
        code: "lsk",
        order: 4,
        getRate: getRateCoinCheck
    },
    {
        name: "ファクトム",
        alias: [],
        code: "fct",
        order: 5,
        getRate: getRateCoinCheck
    },
    {
        name: "モネロ",
        alias: [],
        code: "xmr",
        order: 6,
        getRate: getRateCoinCheck
    },
    {
        name: "オーガー",
        alias: [],
        code: "rep",
        order: 7,
        getRate: getRateCoinCheck
    },
    {
        name: "リップル",
        alias: [],
        code: "xrp",
        order: 8,
        getRate: getRateCoinCheck
    },
    {
        name: "ゼットキャッシュ",
        alias: ["ゼット"],
        code: "zec",
        order: 9,
        getRate: getRateCoinCheck
    },
    {
        name: "ネム",
        alias: [],
        code: "xem",
        order: 10,
        getRate: getRateCoinCheck
    },
    {
        name: "ライトコイン",
        alias: ["ライト"],
        code: "ltc",
        order: 11,
        getRate: getRateCoinCheck
    },
    {
        name: "ダッシュ",
        alias: [],
        code: "dash",
        order: 12,
        getRate: getRateCoinCheck
    },
    {
        name: "ビットコインキャッシュ",
        alias: ["ビッチ"],
        code: "bch",
        order: 13,
        getRate: getRateCoinCheck
    },
    {
        name: "モナコイン",
        alias: ["モナ", "モナコ"],
        code: "mona",
        order: 14,
        getRate: getRateZaif
    }
];

exports.getRate = function(coin) {
    return coin.getRate(coin);
}